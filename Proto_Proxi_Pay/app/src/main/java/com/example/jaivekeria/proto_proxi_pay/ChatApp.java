package com.example.jaivekeria.proto_proxi_pay;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.pubnub.api.Callback;
import com.pubnub.api.Pubnub;
import com.pubnub.api.PubnubError;
import com.pubnub.api.PubnubException;

import java.util.ArrayList;

/**
 * Created by Jai  Vekeria on 5/19/2015.
 */
public class ChatApp extends ActionBarActivity {

    private Button send;
    private EditText mess;
    private ListView v;
    private ArrayAdapter<String> listAdapter ;
    private TextView text;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_layout);

        Parse.initialize(this, "xQVZbtUhLF3bJKRPbnpEdSCo63TCV4x92BcxjVXm", "9FT902wdg8ZvjolzanutL0glmKNxUqNcwIk0LyzI");

        final Pubnub pubnub = new Pubnub("pub-c-b588f3f4-628e-45f0-9159-f98f406273a9", "sub-c-709511ee-fdbb-11e4-af9f-02ee2ddab7fe");

        ParseUser user = ParseUser.getCurrentUser();

        String fname = (String)user.get("First_Name");
        String lname = (String)user.get("Last_Name");
        final String fullName = fname + " " + lname;

        send = (Button)findViewById(R.id.send);
        mess = (EditText)findViewById(R.id.message);
        v = (ListView)findViewById(R.id.listView);
        text = (TextView)findViewById(R.id.name);

        text.setText(getIntent().getExtras().getString("Name"));

        try {
            pubnub.subscribe(getIntent().getExtras().getString("ObjectId"), new Callback() {

                        @Override
                        public void connectCallback(String channel, Object message) {
                            Log.d("PUBNUB","SUBSCRIBE : CONNECT on channel:" + channel
                                    + " : " + message.getClass() + " : "
                                    + message.toString());
                        }

                        @Override
                        public void disconnectCallback(String channel, Object message) {
                            Log.d("PUBNUB","SUBSCRIBE : DISCONNECT on channel:" + channel
                                    + " : " + message.getClass() + " : "
                                    + message.toString());
                        }

                        public void reconnectCallback(String channel, Object message) {
                            Log.d("PUBNUB","SUBSCRIBE : RECONNECT on channel:" + channel
                                    + " : " + message.getClass() + " : "
                                    + message.toString());
                        }

                        @Override
                        public void successCallback(String channel, Object message) {
                            Log.d("PUBNUB","SUBSCRIBE : " + channel + " : "
                                    + message.getClass() + " : " + message.toString());

                            ArrayList<String> madonna = new ArrayList <String>();

                            madonna.add((String)message.toString());

                            listAdapter = new ArrayAdapter<String>(ChatApp.this, R.layout.row, madonna);

                            v.setAdapter(listAdapter);

                        }

                        @Override
                        public void errorCallback(String channel, PubnubError error) {
                            Log.d("PUBNUB","SUBSCRIBE : ERROR on channel " + channel
                                    + " : " + error.toString());
                        }
                    }
            );
        } catch (PubnubException e) {
            Log.d("PUBNUB",e.toString());
        }

        final Callback callback = new Callback() {
            public void successCallback(String channel, Object response) {
                Log.d("PUBNUB",response.toString());
            }
            public void errorCallback(String channel, PubnubError error) {
                Log.d("PUBNUB",error.toString());
            }
        };

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pubnub.publish(fullName + ": " + getIntent().getExtras().getString("ObjectId"), mess.getText().toString(), callback);
                mess.setText("");

           }
        });



    }
}
