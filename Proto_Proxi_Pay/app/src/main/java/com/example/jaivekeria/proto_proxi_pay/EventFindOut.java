package com.example.jaivekeria.proto_proxi_pay;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Jai  Vekeria on 5/16/2015.
 */
public class EventFindOut extends ActionBarActivity
{

    private Button button;
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.find_out);

        Parse.initialize(this, "xQVZbtUhLF3bJKRPbnpEdSCo63TCV4x92BcxjVXm", "9FT902wdg8ZvjolzanutL0glmKNxUqNcwIk0LyzI");

        button = (Button)findViewById(R.id.confirm);
        title =(TextView)findViewById(R.id.title);

        title.setText(getIntent().getExtras().getString("Title"));

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String split3 = getIntent().getExtras().getString("Info");

                Toast.makeText(getApplicationContext(), split3, Toast.LENGTH_LONG).show();

                final ParseUser user = ParseUser.getCurrentUser();

                ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
                query.whereEqualTo("objectId", split3);
                query.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> QRList, ParseException e) {
                        if (e == null) {

                            QRList.get(0).increment("HeadCount");

                            QRList.get(0).addAllUnique("PeopleInfo", Arrays.asList((String) (user.get("First_Name") + " " + user.get("Last_Name") + " " + user.getEmail())));

                            QRList.get(0).saveInBackground();

                            ParsePush.subscribeInBackground(split3, new SaveCallback()
                            {
                                @Override
                                public void done(ParseException e) {
                                if (e == null) {
                                    Toast.makeText(getApplicationContext(), "You have now subscribed for push", Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(getApplicationContext(), "You were not subscribed for push", Toast.LENGTH_LONG).show();
                                }
                            }
                            });

                            Intent intent = new Intent(EventFindOut.this, MainMenu.class);
                            startActivity(intent);

                            Toast.makeText(getApplicationContext(), "You have succesfully registered for this event! " + QRList.size(), Toast.LENGTH_LONG).show();

                         } else {

                             Toast.makeText(getApplicationContext(), "SOMETHING WENT WRONG " + e.getMessage(), Toast.LENGTH_LONG).show();

                             Intent intent = new Intent(EventFindOut.this, MainMenu.class);
                             startActivity(intent);

                         }
                     }
                 });
            }
        });
    }

}
