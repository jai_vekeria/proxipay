package com.example.jaivekeria.proto_proxi_pay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

/**
 * Created by Jai  Vekeria on 5/5/2015.
 */
public class MainMenu extends ActionBarActivity
{
    private Button btnClick;
    private Button openCam;
    private TextView welcome;
    private Button recent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alpha_main_menu);

        Parse.initialize(this, "xQVZbtUhLF3bJKRPbnpEdSCo63TCV4x92BcxjVXm", "9FT902wdg8ZvjolzanutL0glmKNxUqNcwIk0LyzI");

        welcome = (TextView)findViewById(R.id.welcome_back);

        ParseUser currentUser = ParseUser.getCurrentUser();

        String struser = currentUser.getUsername().toString();

        welcome.setText("Welcome to our app " + struser);

        ParsePush.subscribeInBackground("", new SaveCallback() {
            @Override
            public void done(ParseException e) {
              if (e == null) {
                    Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");
                } else {
                    Log.e("com.parse.push", "failed to subscribe for push", e);
                }
            }
        });


        btnClick = (Button)findViewById(R.id.event_create);
        openCam = (Button)findViewById(R.id.event_join);
        recent = (Button)findViewById(R.id.recent);

        //Intent intent = getIntent();

        btnClick.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(MainMenu.this, CreateEvent.class);
                startActivity(intent);
            }
        });

        openCam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainMenu.this, Camera.class);
                startActivity(intent);
            }
        });

        recent.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(MainMenu.this, QRDisplay.class);
                startActivity(intent);
            }
        });
    }
}
