package com.example.jaivekeria.proto_proxi_pay;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.common.BitMatrix;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jai  Vekeria on 5/6/2015.
 */
public class QRDisplay extends ActionBarActivity
{

    private ListView v;
    private ArrayAdapter<String> listAdapter ;
    String objectID;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qr_view);

        Parse.initialize(this, "xQVZbtUhLF3bJKRPbnpEdSCo63TCV4x92BcxjVXm", "9FT902wdg8ZvjolzanutL0glmKNxUqNcwIk0LyzI");

        v = (ListView)findViewById(R.id.menuItems);

        ParseUser CurrentUser = ParseUser.getCurrentUser();
        final String userID = CurrentUser.getObjectId();

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
        query.whereEqualTo("User", userID);

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> QRList, ParseException e) {
                if (e == null)
                {
                    List<String> madonna  = new ArrayList<String>();
                    //Log.d("score", "Retrieved " + scoreList.size() + " scores");
                    ParseQuery query1 = new ParseQuery("Event");
                    for (ParseObject dealsObject : QRList)
                    {
                        madonna.add(dealsObject.get("Name").toString() + " " + dealsObject.getObjectId());
                    }

                    listAdapter = new ArrayAdapter<String>(QRDisplay.this, R.layout.row, madonna);

                    v.setAdapter(listAdapter);

                    Toast.makeText(getApplicationContext(), "Query pull was successful", Toast.LENGTH_LONG).show();
                }else
                {
                    //Log.d("score", "Error: " + e.getMessage());
                    Toast.makeText(getApplicationContext(), "We could'nt find the QRs " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

        v.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long idi)
            {
                Intent intent = new Intent(QRDisplay.this, EventManager.class);
                String selected = ((TextView) view.findViewById(R.id.rowTextView)).getText().toString();
                String[] str = selected.split(" ");

                intent.putExtra("ObjectId", str[1]);
                intent.putExtra("Name", str[0]);
                startActivity(intent);
            }
        });

    }
}
