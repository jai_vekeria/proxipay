package com.example.jaivekeria.proto_proxi_pay;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Jai  Vekeria on 5/3/2015.
 */

public class CreateEvent extends ActionBarActivity {

    private Button btnClick;
    private Button cancel;
    private EditText eName;
    private EditText addr;
    private EditText addin;
    private EditText price;
    private CheckBox free;
    private ImageView QR;
    private TextView td;
    private Button ok;
    private Button can;
    private TimePicker time1;
    private DatePicker date2;
    private Button dattime;
    private ArrayList <String> authUsers;
    private BitMatrix matrix = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_event);

        Parse.initialize(this, "xQVZbtUhLF3bJKRPbnpEdSCo63TCV4x92BcxjVXm", "9FT902wdg8ZvjolzanutL0glmKNxUqNcwIk0LyzI");

        btnClick = (Button)findViewById(R.id.confirm);
        cancel = (Button)findViewById(R.id.cancel);
        eName = (EditText)findViewById(R.id.name);
        addr = (EditText)findViewById(R.id.address);
        price = (EditText)findViewById(R.id.price);
        free = (CheckBox)findViewById(R.id.free);
        addin = (EditText)findViewById(R.id.addinfo);

        final ParseUser currentUser = ParseUser.getCurrentUser();

        free.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked)
                    price.setVisibility(View.VISIBLE);
                else if(!isChecked)
                    price.setVisibility(View.INVISIBLE);
            }
        });

        btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (free.isChecked()) {
                    final ParseObject Event = new ParseObject("Event");

                    authUsers.add(currentUser.getObjectId());

                    List<String> rsvpInfo = new ArrayList<String>();
                    Event.put("Name", eName.getText().toString());
                    Event.put("Address", addr.getText().toString());
                    Event.put("ifEventPayed", free.isChecked());
                    Event.put("Date", addin.getText().toString());
                    Event.put("Price", "0");
                    Event.put("User", currentUser.getObjectId());
                    Event.put("HeadCount", 0);
                    Event.put("PeopleInfo", rsvpInfo);
                    Event.put("AuthorizedUsers", authUsers);


                    Event.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException ex) {
                            if (ex == null) {
                                BitMatrix matrix = null;
                                QRCodeWriter writer = new QRCodeWriter();

                                String id = Event.getObjectId();

                                try {
                                    matrix = writer.encode("ProjectBarProxiPay," + eName.getText().toString() + "," + addr.getText().toString() + "," + id, BarcodeFormat.QR_CODE, 400, 400);
                                } catch (WriterException e) {
                                    e.printStackTrace();
                                }

                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                toBitmap(matrix).compress(Bitmap.CompressFormat.PNG, 100, stream);
                                byte[] data = stream.toByteArray();
                                final ParseFile image = new ParseFile(eName.getText().toString() + addr.getText().toString(), data);

                                ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
                                query.getInBackground(id, new GetCallback<ParseObject>() {
                                    public void done(ParseObject object, ParseException e) {
                                        if (e == null) {
                                            // object will be your game score
                                            object.put("Image", image);
                                            object.saveInBackground(new SaveCallback() {
                                                @Override
                                                public void done(ParseException ex) {
                                                    if (ex == null)
                                                        Toast.makeText(getApplicationContext(), "THE QR WAS CREATED AND UPLOADED TO OUR SERVERS", Toast.LENGTH_LONG).show();
                                                    else
                                                        Toast.makeText(getApplicationContext(), "SOMETHING WENT WRONG ", Toast.LENGTH_LONG).show();
                                                }
                                            });
                                        } else
                                            Toast.makeText(getApplicationContext(), "SOMETHING WENT WRONG ", Toast.LENGTH_LONG).show();
                                    }
                                });
                            } else {
                                Toast.makeText(getApplicationContext(), "SOMETHING WENT WRONG " + ex.getMessage(), Toast.LENGTH_LONG).show();

                            }
                        }
                    });
                } else if (!free.isChecked()) {
                        /*price.setVisibility(view.VISIBLE);
                        ParseObject Event = new ParseObject("Event");
                        matrix = writer.encode("ProjectBarProxiPay, " + eName.getText().toString() + ", " + price.getText().toString() + ", " +  addin.getText().toString() + ", " + Event.getObjectId(), BarcodeFormat.QR_CODE, 400, 400);
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        toBitmap(matrix).compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] data = stream.toByteArray();
                        ParseFile image = new ParseFile(eName.getText().toString() + addr.getText().toString(), data);
                        image.saveInBackground();

                        ArrayList<String> rsvpInfo = new ArrayList<String>();
                        final ParseObject Event = new ParseObject("Event");

                        Event.put("Name", eName.getText().toString());
                        Event.put("Address", addr.getText().toString());
                        Event.put("ifEventPayed", free.isChecked());
                        Event.put("Date", addin.getText().toString());
                        Event.put("Price", price.getText().toString());
                        Event.put("User", currentUser.getObjectId());
                        Event.put("HeadCount", 0);
                        Event.put("PeopleInfo", rsvpInfo);

                        Event.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException ex) {
                                if (ex == null) {
                                    QRCodeWriter writer = new QRCodeWriter();
                                    String id = Event.getObjectId();
                                    try {
                                        matrix = writer.encode("ProjectBarProxiPay," + eName.getText().toString() + "," + price.getText().toString() + "," + addin.getText().toString() + "," + id, BarcodeFormat.QR_CODE, 400, 400);
                                    }catch (WriterException e) {
                                        e.printStackTrace();
                                    }
                                    //Status.setText("Here is the code for your event");
                                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                    toBitmap(matrix).compress(Bitmap.CompressFormat.PNG, 100, stream);
                                    byte[] data = stream.toByteArray();
                                    final ParseFile image = new ParseFile(eName.getText().toString() + addr.getText().toString(), data);
                                    ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
                                    query.getInBackground(id, new GetCallback<ParseObject>() {
                                        public void done(ParseObject object, ParseException e) {
                                            if (e == null) {
                                                // object will be your game score
                                                object.put("Image", image);
                                                object.saveInBackground(new SaveCallback() {
                                                @Override
                                                public void done(ParseException ex) {
                                                    if (ex == null) {
                                                        Toast.makeText(getApplicationContext(), "THE QR WAS CREATED AND UPLOADED TO OUR SERVERS", Toast.LENGTH_LONG).show();
                                                    } else {

                                                    }
                                                }
                                            });
                                        } else {
                                            // something went wrong
                                        }
                                    }
                                });
                            } else {
                                Toast.makeText(getApplicationContext(), "SOMETHING WENT WRONG " + ex.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });*/
                }
            }

        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Process Canceled", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(view.getContext(), MainMenu.class);
                startActivityForResult(intent, 0);
            }
        });
    }

    public static Bitmap toBitmap(BitMatrix matrix)
    {
        int height = matrix.getHeight();
        int width = matrix.getWidth();
        Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                bmp.setPixel(x, y, matrix.get(x,y) ? Color.BLACK : Color.WHITE);
            }
        }
        return bmp;
    }
 }


