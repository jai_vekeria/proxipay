package com.example.jaivekeria.proto_proxi_pay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

/**
 * Created by Jai  Vekeria on 5/5/2015.
 */
public class SignUp extends ActionBarActivity
{
    private Button btnClick;
    private EditText first;
    private EditText last;
    private EditText email;
    private EditText pass;
    private EditText userN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up);

        Parse.initialize(this, "xQVZbtUhLF3bJKRPbnpEdSCo63TCV4x92BcxjVXm", "9FT902wdg8ZvjolzanutL0glmKNxUqNcwIk0LyzI");

        Intent intent = getIntent();

        btnClick = (Button)findViewById(R.id.submit);
        first =(EditText)findViewById(R.id.firstName);
        last =(EditText)findViewById(R.id.lastName);
        email =(EditText)findViewById(R.id.email);
        pass =(EditText)findViewById(R.id.passW);
        userN = (EditText)findViewById(R.id.userName);

        btnClick.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {

                if (userN.equals("") && pass.equals("") && first.equals("") && last.equals("") && email.equals("")) {
                    Toast.makeText(getApplicationContext(), "Please complete the sign up form", Toast.LENGTH_LONG).show();
                } else {
                    ParseUser user = new ParseUser();
                    user.setUsername(userN.getText().toString());
                    user.put("First_Name", first.getText().toString());
                    user.put("Last_Name", last.getText().toString());
                    user.setPassword(pass.getText().toString());
                    user.setEmail(email.getText().toString());

                    user.signUpInBackground(new SignUpCallback() {
                        public void done(ParseException e) {
                            if (e == null) {
                                // Hooray! Let them use the app now.
                                Toast.makeText(getApplicationContext(), "Cool. SignUp was a success!", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(SignUp.this, MainMenu.class);
                                startActivity(intent);
                            } else {
                                Toast.makeText(getApplicationContext(), "Something went wrong " + e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                }
            }
        });



    }
}
