package com.example.jaivekeria.proto_proxi_pay;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.View;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class MainActivity extends ActionBarActivity
{

    private Button btnClick;
    private Button signUp;
    EditText userName;
    EditText passWord;

    //private TextView Status;
    //private ImageView QR;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log_in);
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Initilize parse with the project key
        Parse.initialize(this, "xQVZbtUhLF3bJKRPbnpEdSCo63TCV4x92BcxjVXm", "9FT902wdg8ZvjolzanutL0glmKNxUqNcwIk0LyzI");

        //Get XML components
        btnClick = (Button)findViewById(R.id.login);
        signUp = (Button)findViewById(R.id.sign_up);
        userName = (EditText)findViewById(R.id.userName);
        passWord = (EditText) findViewById(R.id.passText);

        //Listener for the login button
        btnClick.setOnClickListener(new View.OnClickListener()
        {
            @Override

            public void onClick(View view)
            {
                ParseUser.logInInBackground(userName.getText().toString(), passWord.getText().toString(), new LogInCallback() {
                    public void done(ParseUser user, ParseException e) {
                        if (user != null) {
                            // Hooray! The user is logged in.
                            Toast.makeText(getApplicationContext(), "Cool. Login was a success!", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(MainActivity.this, MainMenu.class);
                            startActivity(intent);
                        } else {
                            // Signup failed. Look at the ParseException to see what happened.
                            Toast.makeText(getApplicationContext(), "No such user exist, please signup" + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
        //////////////////////////////////////////////
        //Listener for the signup button
        signUp.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Toast.makeText(getApplicationContext(), "Sending you to the Sign Up Page, please wait....", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(view.getContext(), SignUp.class);
                startActivityForResult(intent, 0);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
