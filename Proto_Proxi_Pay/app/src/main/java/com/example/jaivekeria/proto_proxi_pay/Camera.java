package com.example.jaivekeria.proto_proxi_pay;

import android.app.Dialog;
import android.app.usage.UsageEvents;
import android.content.Context;
import android.content.Intent;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.google.zxing.*;

import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.regex.Pattern;



//import eu.livotov.zxscan.ScannerView;


/**
 * Created by Jai  Vekeria on 5/5/2015.
 */
public class Camera extends ActionBarActivity implements QRCodeReaderView.OnQRCodeReadListener {
    private TextView qrin;
    private String war = null;
   // private ZXingScannerView mScannerView;
   private QRCodeReaderView mydecoderview;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera);

        Parse.initialize(this, "xQVZbtUhLF3bJKRPbnpEdSCo63TCV4x92BcxjVXm", "9FT902wdg8ZvjolzanutL0glmKNxUqNcwIk0LyzI");

        mydecoderview = (QRCodeReaderView) findViewById(R.id.qrdecoderview);
        mydecoderview.setOnQRCodeReadListener(Camera.this);
    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {

        final ParseUser user = ParseUser.getCurrentUser();

        final String[] split = text.split(",");

        if ((split.length == 4) && (split[0].equals("ProjectBarProxiPay")))
        {
            Intent intent = new Intent(Camera.this, EventFindOut.class);
            intent.putExtra("Info", split[3]);
            intent.putExtra("Title", split[1]);
            startActivity(intent);

            //Toast.makeText(getApplicationContext(), split[3], Toast.LENGTH_LONG).show();

        }
    }


    // Called when your device have no camera
    @Override
    public void cameraNotFound() {

    }

    // Called when there's no QR codes in the camera preview image
    @Override
    public void QRCodeNotFoundOnCamImage() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        mydecoderview.getCameraManager().startPreview();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mydecoderview.getCameraManager().stopPreview();
    }

     @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {

    }

 }



