package com.example.jaivekeria.proto_proxi_pay;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;

/**
 * Created by Jai  Vekeria on 5/16/2015.
 */
public class EventManager extends ActionBarActivity
{
    private TextView pop;
    private TextView list;
    private ImageView qr;
    private Button sendPush;
    private TextView title;
    private Button startChat;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_manager);

        Parse.initialize(this, "xQVZbtUhLF3bJKRPbnpEdSCo63TCV4x92BcxjVXm", "9FT902wdg8ZvjolzanutL0glmKNxUqNcwIk0LyzI");

        //ParseUser user = ParseUser.getCurrentUser();

        final String objectId = getIntent().getExtras().getString("ObjectId");
        final String name = getIntent().getExtras().getString("Name");

        pop = (TextView)findViewById(R.id.gnum);
        list = (TextView)findViewById(R.id.glist);
        qr = (ImageView)findViewById(R.id.img);
        sendPush = (Button)findViewById(R.id.ann);
        title = (TextView)findViewById(R.id.title);
        startChat = (Button)findViewById(R.id.chat);

        title.setText(name);

        sendPush.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view)
             {
                 AlertDialog.Builder alert = new AlertDialog.Builder(EventManager.this);
                 alert.setTitle("PUSH");
                 alert.setMessage("Enter Your Message Here");

                 final EditText input = new EditText(EventManager.this);
                 alert.setView(input);

                 alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                     public void onClick(DialogInterface dialog, int whichButton) {
                         ParsePush push = new ParsePush();
                         push.setChannel(objectId);
                         push.setMessage(input.getText().toString());
                         push.sendInBackground();
                         Toast.makeText(EventManager.this,"Message was sent!", Toast.LENGTH_LONG).show();
                     }
                 });
                 alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                     public void onClick(DialogInterface dialog, int whichButton) {
                         // Canceled.
                         dialog.cancel();
                     }
                 });
                 AlertDialog alertDialog = alert.create();
                 alertDialog.show();
             }
        });

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
        query.getInBackground(objectId, new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    pop.setText(object.get("HeadCount").toString());
                    list.setText(object.get("PeopleInfo").toString());

                    ParseObject fileHolder = null;

                    fileHolder = object;
                    ParseFile bum = (ParseFile) fileHolder.get("Image");
                    byte[] file = new byte[0];
                    try {
                        file = bum.getData();
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                    Bitmap image = BitmapFactory.decodeByteArray(file, 0, file.length);
                    qr.setImageBitmap(image);

                } else {

                }
            }
        });

        startChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(EventManager.this, ChatApp.class);

                intent.putExtra("ObjectId", objectId);
                intent.putExtra("Name", name);

                startActivity(intent);
            }
        });
    }

}
